#pragma once

#include <Windows.h>
#include "sdk/BSLib/GfxRuntimeClass.h"
#include "sdk/gfx/GFXMainFrame/CMsgHandler.h"
#include "sdk/gfx/GFXMainFrame/ObjChild.h"
#include "sdk/gfx/GFX3DFunction/Camera.h"


class CGFXMainFrame : public CObjChild, public CMsgHandler {
public:
	virtual char F8(int a1); // maybe init (cgame implementation loads settings)
	virtual char F9(); // returns 1 in this impl
	virtual void Cycle();
	virtual void SetNextProcess(CGfxRuntimeClass*); // this func is also referenced by address. WTF.
	virtual void Renderer(); // wat?
	virtual char RenderFrame();
	virtual void F14();
	virtual void F15(); // asserts if m_blDrawing == 0
	virtual char VirtualApplyDepthBufferPriority(int a1); // returns 0
	virtual int F17(int a1); // returns 1
	virtual char F18(int a1); // returns 0

	HWND hwnd() const;
	static void SetNextProcessSTAT(CGfxRuntimeClass* cls);
	
public:
	char pad_0024[16]; //0x0024
	HWND mainHWND; //0x0034
	HINSTANCE mainInstance; //0x0038
	char pad_003C[4]; //0x003C
	bool b_RenderingPossible; //0x0040
	char pad_0041[763]; //0x0041
	CCamera camera; //0x033C
};
