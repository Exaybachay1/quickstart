#include "unsorted.h"

#include <Windows.h>



bool TryCreateCompatibleDC()
{
  g_hdc = CreateCompatibleDC(0);
  return g_hdc != 0;
}

bool NetworkConnect()
{
	return reinterpret_cast<bool(*)()>(0x008449F0)();
}
