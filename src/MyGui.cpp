#include "MyGui.h"

#include "sdk/gfx/imgui/imgui.h"

#define g_DebugMode (*reinterpret_cast<int*>(0x00EED6A7))

void MyGui::MainMenuBar()
{
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			ImGui::MenuItem("Debug Mode", NULL, reinterpret_cast<bool*>(0x00EED6A7));
			ImGui::EndMenu();
		}

		ImGui::EndMainMenuBar();
	}
}


void MyGui::DoWork() {
	MainMenuBar();
}

