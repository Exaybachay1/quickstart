#pragma once
#include "sdk/gfx/GFXMainFrame/GFXMainFrame.h"

class CGame : public CGFXMainFrame {
public:
	void InitGameAssets();
	void LoadTextfiles();
	void ResizeMainWindow();

	char pad_0490[908]; //0x0490
	CGfxRuntimeClass* m_runtimeClass; //0x081C
	char pad_0820[320]; //0x0820

};

#define g_CGame ((CGame*)0x00EECE80)
